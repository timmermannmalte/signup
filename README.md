Allgemeine Konfiguration.

Innerhalb des Ordners "src/conf" (dbconf.php) muss einmal der Pfad zur SQLite angegeben werden. -- aktuell wird nur SQLite unterstützt.
Da die Datenbank auf SQLite basiert habe ich diese mit hinzugefügt.Dennoch ist ein Export der Db Struktur im Ordner "db" vorhanden.

Desweiteren muss in der "apiconf.php" die URL zur API angegeben werden. -- da ich diese nicht einfach mit veröffentlichen wollte.



1. Describe possible performance optimizations for your Code.
- Die eingebundenen JS Dateien könnten noch minifiziert werden, damit weniger Daten zum Clienten übermittelt werden müssen. Dies würde auch den Vorteil bringen, dass der Code nicht mehr so einfach von außen zu lesen ist.


2. Which things could be done better, than you´ve done it?
- Zur Verbindung und Kommunikation mit der DB sollte man, wenn noch andere DatenbankArten unterstützt werden sollen, auf ein ORM (Doctrine oder ähnliches) umstellen, um so datenbankspezifische Dinge nicht beachten zu müssen und flexibler zu sein.

- Des weiteren sollte das allgemeine Routing auf schönere URLs(Clean URLs) umgestellt werden. Dazu sind dann jedoch auch Anpassungen innerhalb des Servers notwendig um alle Anfragen auf eine Datei umzulenken.

- Die Datenbankstruktur ist nur für die angegebene Aufgabe erfüllt. Falls es z.B. möglich sein sollte, mehrere Adressen oder Zahlungsmöglichkeiten zu speichern, müssten diese Informationen in eine eigene Tabelle ausgelagert werden.

- Die Fehlerbehandlung könnte früher in der Oberfläche erfolgen, um dem Nutzer schneller und präzisere Rückmeldungen geben zu können.

- Allgemein sollte, wenn das System weiter wächst, TestRoutinen eingefügt werden, um schneller zu erkennen ob Änderungen am Code oder an bestimmten Abschnitten zu Problemen führen können