
class PersonalInformation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return React.createElement(
            "div",
            { className: "step" },
            React.createElement("input", {
                className: "form-control",
                placeholder: "firstname",
                name: "firstname",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.firstname
            }),
            React.createElement("input", {
                className: "form-control",
                placeholder: "lastname",
                name: "lastname",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.lastname
            }),
            React.createElement("input", {
                className: "form-control",
                placeholder: "telephone",
                name: "telephone",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.telephone
            }),
            React.createElement("input", {
                className: "form-control",
                type: "submit",
                value: "Next",
                onClick: this.props.handleNext,
            })
        );
    }
}

class AddressInformation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return React.createElement(
            "div",
            { className: "step" },
            React.createElement("input", {
                className: "form-control",
                placeholder: "street",
                name: "street",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.street
            }),
            React.createElement("input", {
                className: "form-control",
                placeholder: "housenumber",
                name: "housenumber",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.housenumber
            }),
            React.createElement("input", {
                className: "form-control",
                placeholder: "zipcode",
                name: "zipcode",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.zipcode
            }),
            React.createElement("input", {
                className: "form-control",
                placeholder: "city",
                name: "city",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.city
            }),
            React.createElement("input", {
                className: "form-control",
                type: "submit",
                value: "Prev",
                onClick: this.props.handlePrev,
            }),
            React.createElement("input", {
                className: "form-control",
                type: "submit",
                value: "Next",
                onClick: this.props.handleNext,
            })
        );
    }
}

class PaymentInformation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return React.createElement(
            "div",
            { className: "step" },
            React.createElement("input", {
                className: "form-control",
                placeholder: "accountowner",
                name: "accountowner",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.accountowner
            }),
            React.createElement("input", {
                className: "form-control",
                placeholder: "iban",
                name: "iban",
                onChange: this.props.handleChange,
                defaultValue: this.props.state.data.iban
            }),
            React.createElement("input", {
                className: "form-control",
                type: "submit",
                value: "Prev",
                onClick: this.props.handlePrev,
            }),
            React.createElement("input", {
                className: "form-control",
                type: "submit",
                value: "Save",
                onClick: this.props.handleSubmit,
            })
        );
    }
}

class SuccessPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return React.createElement(
            "div",
            { className: "step" },
            React.createElement('div', null,
            'The data were stored successfully. Your payment ID:'+ this.props.state.data.paymentDataId)
        );
    }
}

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stepNumber: 1,
            data: {
                firstname: '',
                lastname: '',
                telephone: '',
                street: '',
                housenumber: '',
                zipcode: '',
                city: '',
                accountowner: '',
                iban: '',
            }
        };


        let savedData = {};
        Object.keys(this.state.data).map(function(objectKey, value) {
            savedData[objectKey] = localStorage.getItem(objectKey);
        });
        if(_.size(savedData) > 0){
            this.state.data = savedData;
        }
        if(localStorage.getItem('stepNumber') !== null){
          this.state.stepNumber = parseInt(localStorage.getItem('stepNumber'));
        }

        this.handleChange = this.handleChange.bind(this);

        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({data :{...this.state.data, [e.target.name]: e.target.value }})
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem(e.target.name,e.target.value);
        }
    }

    handlePrev(event) {
        this.setState({ stepNumber: --this.state.stepNumber });
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem('stepNumber',this.state.stepNumber);
        }
    }

    handleNext(event) {
        this.setState({ stepNumber: ++this.state.stepNumber });
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem('stepNumber',this.state.stepNumber);
        }
    }

    handleSubmit(event) {
        var obj = this;

        $.ajax({
            type: "POST",
            url: 'php/index.php?controller=signup&action=saveSignup',
            data: {formData: obj.state},
            dataType: 'json'
          }).done(function(data) {
            if(data.success == true){
                obj.setState({data :{...obj.state.data, paymentDataId: data.paymentDataId  }})
                obj.setState({ stepNumber: ++obj.state.stepNumber });
            }else{
                alert('Invalid data. Please check your entries or try again later')
            }
          })
          .fail(function(data) {
            console.log(data);
          })
          .always(function(data) {
            console.log(data);
          });

        //this.setState({ stepNumber: ++this.state.stepNumber });
    }



    currentView() {
        switch (this.state.stepNumber) {
            case 1:
                return PersonalInformation
            case 2:
                return AddressInformation
            case 3:
                return PaymentInformation
            case 4:
                return SuccessPage
            default:
                break;
        }
    }

    render() {
        return (
            React.createElement(
                "div",
                { className: "MarkdownEditor" },
                React.createElement(
                    "h3",
                    null,
                    "Signup Step" + this.state.stepNumber
                ),
                React.createElement(this.currentView(), this, null),
            )
        );
    }
}
ReactDOM.render(React.createElement(SignUp, null), document.querySelector('#signup-steps'));