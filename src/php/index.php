<?php
/**
 * Allgemein wird ein Request in Form auf die Seite index.php?controller=ControllerName&action=FunktionsName
 * Dabei wird das Controller immer automatisch angehangen.
 * Bei der Funktion wird immer Action angehangen
 */
if(isset($_GET['controller']) && file_exists('Controller/'. ucfirst($_GET['controller']) . 'Controller.php')){
    $controllerName = ucfirst($_GET['controller']) . 'Controller';
    require_once 'Controller/'. $controllerName . '.php';

    $controller = new $controllerName();


    if(isset($_GET['action']) && method_exists( $controller, $_GET['action'] . 'Action')){
        $actionName = $_GET['action'] . 'Action';
        try {
            sendJsonReponse(
                $controller->$actionName()
            );
        } catch (\Exception $exp) {
            $response = array(
                'success' => false,
                'msg' => $exp->getMessage(),
             );
             sendJsonReponse($response);
        }
    }else{
        $response = array(
            'success' => false,
            'msg' => 'Invalid Action given',
         );
         sendJsonReponse($response);
    }


}else{
    $response = array(
       'success' => false,
       'msg' => 'Invalid Controller given',
    );
    sendJsonReponse($response);
}


function sendJsonReponse($response){
    header('Content-Type: application/json');
    echo json_encode($response);
}
