<?php
require_once('Service/DBConnectorService.php');

/**
 * Model zur Pruefung und Verarbeitung der Calls fuer die Registrierung
 */
class SignupModel
{
	/**
	 * Speichert die RegistrierungsDaten
	 *
	 * @param array $formData
	 * @return array Response
	 */
	public function saveSignup($formData)
	{
		$dbConnect = new DBConnectorService();
		$insertedId = $dbConnect->insertRowForTable('customer', $formData);
		$paymentDataId = $this->getPaymentDataId($insertedId, $formData['iban'], $formData['accountowner']);

		if($paymentDataId !== false){
			$dbConnect->updateRowForTable('customer', $insertedId, array('paymentdataid' => $paymentDataId));
			return array(
				'success' => true,
				'message' => 'The data could be stored successfully.',
				'paymentDataId' => $paymentDataId,
			);
		}else{
			return array(
				'success' => false,
				'message' => 'Error while generating the paymentDataId.',
			);
		}
	}

	/**
	 * Frag mithilfe der gespeicherten Daten und der neuen ID die paymentDataID ab.
	 *
	 * @param int $insertedId AutoInc aus der DB
	 * @param string $iban uebergebene IBAN
	 * @param string $accountOwner
	 * @return mixed paymentDataId sofern diese abgefragt werden konnte, ansonsten false
	 */
    public function getPaymentDataId ($insertedId, $iban, $accountOwner)
    {
		if(!empty($insertedId) && !empty($iban) && !empty($accountOwner)){
			$jsonData = array(
				"customerId"=> $insertedId,
				"iban"=> $iban,
				"owner"=> $accountOwner,
			);

			include_once '../conf/apiconf.php';
			$ch = curl_init($apiURL['paymentIdDataURL']);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonData));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen(json_encode($jsonData)))
			);

			$result = curl_exec($ch);
			if($result !== false){
				$responseData = json_decode($result,true);
				if(isset($responseData['paymentDataId'])){
					return $responseData['paymentDataId'];
				}
			}
		}
		return false;
	}

}

