<?php
require_once('Model/SignupModel.php');

/**
 * Controller zur Annahme der Calls fuer die Registrierung
 */
class SignupController
{

    protected $signupModel = null;

    public function __construct() {
        $this->signupModel = new SignupModel();
    }

    /**
     * Nimmt die Daten für die Speicherung der Registrierung an
     * und gibt bein Erfolg die paymentDataId yurueck
     *
     * @return array  Response
     */
    public function saveSignupAction()
    {
        $formData = $_POST['formData']['data'];
        return $this->signupModel->saveSignup($formData);
    }
}
