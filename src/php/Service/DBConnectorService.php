<?php

/**
 * Handelt die Connection und Abfrage auf die DB
 * Zu erweitern wenn auch andere DB Arten unterstuetzt werden sollen.
 * ggf. sollte dann auch auf ein ORM umgestellt werden.
 */
class DBConnectorService
{
    protected $connection = null;

    /**
     * Baut die Connection für die Datenbank auf.
     * Aktuell wird nur SQLite unterstuetzt
     */
    public function __construct() {
        include_once '../conf/dbconf.php';
        $this->connection = new PDO($dbConnection['path']);
    }

    /**
     * Fuegt die uebergebenen Daten in die angegebene Tabelle ein
     *
     * @param string $tableName Tabellennamen in welcher die Daten geschrieben werden sollen
     * @param array $data ($columnName => $data), array fuer alle werte die gespeichert werden sollen
     * @return int AutoInc Nummer der eingegebenen Zeile
     */
    public function insertRowForTable($tableName, $data){
        $insertStmt = "INSERT INTO ".$tableName;

        $insertStmt .= "('". implode("','", array_keys($data)) . "')";
        $insertStmt .= " VALUES ('". implode("','", $data) . "')";

        $this->connection->query($insertStmt);
        return $this->connection->lastInsertId();
    }

    /**
     * Updatet die Daten fuer die uebergebenen Tabelle, wobei der eindeutige Schluessel mitgegeben werden muss.
     * BAsiert darauf das jede Tabelle als Schluessel eine eindeutige id Spalte hat.
     *
     * @param string  $tableName Tabellenname
     * @param int $key eindeutiger Schluessel innerhalb der Tabelle (id)
     * @param array $data ($columnName => $data), array fuer alle werte die veraendert/gespeichert werden sollen
     * @return void
     */
    public function updateRowForTable($tableName, $key, $data){
        $updateStmt = "UPDATE ".$tableName . " SET ";

        foreach($data as $columnName => $value){
            $updateStmt .= $columnName . " = '" . $value . "'";
        }
        $updateStmt .= " where id = " . $key;

        $this->connection->query($updateStmt);
    }
}

