BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `customer` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`firstname`	TEXT,
	`lastname`	TEXT,
	`telephone`	TEXT,
	`street`	TEXT,
	`housenumber`	TEXT,
	`zipcode`	TEXT,
	`city`	TEXT,
	`accountowner`	TEXT,
	`iban`	TEXT,
	`paymentdataid`	TEXT
);
COMMIT;
